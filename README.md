# rolling in the mud                    179 pt


## Description

uugh, these pigs in my pen are making a complete mess! They're rolling all over the place!

Anyway, can you decode this cipher they gave me, almost throwing it at me while rolling around?

Answer in lowercase with symbols. In the image, { and } are characters that should appear in your flag, and replace spaces with _.

files : cipher.png

## Analyse

When we look the picture, after have reading the description, i instantly see a flag, that was renversed. It's really seems to byte a mono-alphabetic cypher

## Method 

Because it's mono-alphabetic cypher, we can distingue structure in symbols exactly like letters in words. We can subsitute letter, begining with the knowed char lactf.

Then, the game is to guess each character. Because the description use the word rolling, pigs, etc, we can guess the flag.

## Real Method

After CTF, i realise it's juste the Pigpen cipher (i didn't knew this), with an extrat rotation of chars.